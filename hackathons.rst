Hackathons
##########

HackCU is pretty easy. Make sure everyone has a ride there and back and remind
people to sign up.

The out of state Hackathon is a lot of work.

1. Over the summer, contact Hackathon organizers and see if we can get any kind
   of financial help. They probably won't pay for flights but in 2019 we were
   able to get transportation paid for to/from the hotel and event using Uber.
2. Flights should be booked a month in advance. This means having a list of
   people who are going within the first few meetings.
3. Transportation once you're there might be challenging. Uber works well although
   it's a tad expensive.
4. Hotels should be booked early, but we did it a week out for 2019 and were alright.
5. Keep up constant communication. Otherwise, people might drop or make other
   plans.
6. Traveling to DIA took a while. We left at 5:45 for a 9:30 flight in 2019 and
   that wasn't enough time. The shuttle likely takes an additional 30 minutes
   once you arrive.
7. People generally didn't have problems packing everything into a single bag.
   We had a couple of duffel bags which were nice because we didn't have to
   check them on the way and could just fold them up and put them in other bags.
8. Have all receipts ready to go. If you don't have a receipt with a
   confirmation number, you don't have a reservation.
