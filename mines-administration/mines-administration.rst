Mines Administration
====================

:Owner: Sumner Evans

.. toctree::
   :glob:
   :maxdepth: 1
   :caption: Contents:

   forms/forms.rst

BSO
---

**The BSO forms are extremely important. We loose funding if we do not do these
forms!**

We need to make sure that we do at least 4 :ref:`service events
<service-events>`. One of these service events must be coordinated with another
club. It is normally easiest to coordinate with LUG.

Some of our responses to past years forms are included in the `BSO Forms`_
directory.

.. _BSO Forms: forms/forms.html

SAIL
----

TODO (Sumner): figure out what needs to be done.

CS Department Funding
---------------------

Once we run out of money from the BSO, we can start pulling from C-MAPP funding.

TODO (Sam): figure this out
