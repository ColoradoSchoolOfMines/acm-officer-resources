Service Events Form 2018-19
===========================

:Date Completed: 3/7/2019 6:13 PM

Partnership Event
-----------------

- Community Event
- **Event Title**: Shelton Math and Science Night
- **Partner Organization**: Mines Linux Users Group
- **Partner Contact**: jgarner@mines.edu
- **Date event was/will be held**: 10/18/2019
- **Purpose & Intended Audience**:

  The goal of the event was to engage the local Golden community by showing
  elementary school children the possibilities of Computer Science. Our goal was
  to expose them to a field which they likely did not know much about before the
  event. Additionally, the event is a great opportunity for the Mines reputation
  to spread in the broader community.

- **Attendance**:

  We had 10 people from our clubs in attendance. There were ~150 people total in
  attendance from the Golden community.

- **Advertising**:

  Since the event was hosted by Shelton Elementary School, they handled
  advertising the event to the school's community. On our end, we announced the
  service opportunity in our club meetings and in our weekly emails for multiple
  weeks.

- **Organization/Planning**:

  The presidents of both clubs communicated to ensure that both clubs had
  sufficient members in attendance and that the activities we brought would be
  engaging for elementary school students.

- **Cost Breakdown**:

  The cost was split 50-50.

- **Successes/Changes**:

  The event was a great opportunity for us to engage the community and show
  children the possibilities and breadth of what computer science can do for
  you.

Service Event 1
---------------

- Community Event
- **Event Title**: High School Programming Competition
- **Date event was/will be held**: April 13, 2019
- **Purpose & Intended Audience**:

  We are partnering with the CS department to host a high school programming
  competition where we invite teams from local high schools to come and compete
  in a programming competition modeled after the International Collegiate
  Programming Contest (ICPC). ACM members write all of the problems for the
  competition, and we volunteer to run the actual event.

- **Attendance**:

  We expect to have 70-100 participants (depending on how many schools
  participate). Since this is such a large event, we expect to have 10 or more
  volunteers from our club helping to run the event. Additionally, we have
  around 5 people working on problems for the competition, and others are
  helping to vet the problems. We are also actively working with a world renown
  competition host to run our competition.

- **Advertising**:

  We are working with the CS Department at Mines to advertise the event. As this
  is primarily school-to-school advertising through our department head's
  connections, we have taken a background role in advertising by performing
  actions such as setting up a website for advertising the competition.

- **Organization/Planning**:

  The president is responsible for organizing writing the problems for the
  competition. The president is working closely with the officer team and the CS
  department to ensure that there are enough volunteers to help with the event.

- **Successes/Changes**:

  We hosted the competition last year, and it went very smoothly overall. The
  competition was hard, but too many teams completed all of the problems, so we
  intend to make it more difficult this year to ensure that everyone is engaged
  for the entire event. Additionally, we have worked with the CS department to
  spread the event to two computer labs since it was very crowded having only
  one last year.

Service Event 2
---------------

- Campus Event
- **Event Title**: Software Engineering Workshop Series
- **Date event was/will be held**: Every other Monday throughout Fall 2018
- **Purpose & Intended Audience**:

  Many students in the CS@Mines community would like to work on projects, but do
  not have the tools to do so. Mines ACM ran a series of workshops in Fall 2018
  to teach students industry-standard collaboration technologies and methods.
  During these workshops, students were taught in a hands-on way where they were
  able to apply what they learned to a real project.

- **Attendance**:

  We had 5 students from our club actively working with around 15 participants.

- **Advertising**:

  We advertised this series of workshops in our mailing list, in classes such as
  Software Engineering, and at Celebration of Mines.

- **Organization/Planning**:

  The entire officer team (4 people), as well as Liam Warfield (one of our core
  members) ran the workshops. We also had other members helping with the
  workshops in non-organizational roles.

- **Successes/Changes**:

  We had the opportunity to speak to many of the people who participated in the
  workshops and they found it was a great educational experience. Many of those
  people were able to join larger projects at ACM and are now equipped with the
  tools necessary for internships and jobs. We plan to do this same workshop
  series next year. We will continue working to make the content relevant and
  engaging to the workshop participants.

Service Event 3
---------------

- Campus Event
- **Event Title**: ALAMODE Linux Lab cleaning
- **Date event was/will be held**: October 5, 2018
- **Purpose & Intended Audience**:

  ALAMODE is a public Linux lab in the basement of Brown Building which is used
  constantly by student from all disciplines. Because of the foot traffic,
  ALAMODE can become messy at times, and ACM organized an event to clean up the
  room. We cleaned the whiteboards, ensured that all of the computers were
  working. We also reorganized some of the furniture in the lab.

- **Attendance**:

  We had 13 students help us clean up the lab.

- **Advertising**:

  We solicited help from our members using our mailing list.

- **Organization/Planning**:

  The vice chair was in charge of organizing the event.

- **Successes/Changes**:

  After the event, many people said they were appreciative of the newly cleaned
  lab. We intend to make it a regular service event in the future.
