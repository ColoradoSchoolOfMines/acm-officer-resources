Goals Form 2018
===============

:Date Completed: 10/23/2018 2:35 PM

- **What is the purpose of this organization?**

  The mission of ACM is to help students learning about technology through
  interesting projects and technical talks.

- **First goal**

  On-Campus Outreach

  On campus, we want to reach out to new, current, and potential CS majors. We
  will reach out to people in Software Engineering and show them how we can help
  them learn things like Git, GitHub, etc. which are valuable for their
  coursework, as well as getting internships.

- **Second goal**

  Off-Campus Outreach

  Off campus, we want to do the Sheldon Math and Science night again, but we
  want to do one or two more service events for a total of 2 or 3 next year.

- **Third goal**

  Diversity

  Our goal is to achieve a gender ratio at ACM equal or better than the CS@Mines
  gender ratio. To achieve this goal, we will work with ACM-W to encourage
  cross-attendance, and work to ensure that we are welcoming to new attendees.
