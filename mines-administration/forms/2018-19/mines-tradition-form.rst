Mines Tradition Form (2018-2019)
================================

:Date Completed: 3/7/2019 6:18 PM

- **What was the nature of your participation in Celebration of Mines?**

  We had multiple old computers at our booth where students could sign up for
  our mailing list. We engaged with new students and gave them stickers and
  flyers describing our club. We also worked with the CS Department to give
  students in CSCI 101 extra credit for coming by our booth.
