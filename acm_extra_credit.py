#!/usr/bin/python

# A useful little script for generating a CSV of club members using the survey
# output of acmwebsite/mozzarella.
#
# Make sure to install csmdirsearch from pip.
#
# The authtkt cookie can be found by logging into `acm.mines.edu` as an
# admin, and checking the cookie storage for "authtkt".


import requests
import argparse
import csv
import csmdirsearch

FIELD = 'Software Engineering'

parser = argparse.ArgumentParser()
parser.add_argument('auth', type=str, help='authtkt cookie value for an acmwebsite admin')
parser.add_argument('first', type=int, help='the first survey # to use')
parser.add_argument('last', type=int, help='the last survey # to use')
parser.add_argument('output', type=argparse.FileType('w'), help='the output csv file')
args = parser.parse_args()
numPeople = {}

def get_responses(cookie, ids):
    s = requests.Session()
    cookies = { 'authtkt': cookie }
    people = []
    for index in ids:
        try:
            r = s.get(f'https://acm.mines.edu/s/{index}/results.json', cookies=cookies)
            if not r.ok:
                print(f'!! could not load survey #{index}')
            for person in r.json()['responses']:
                print(person)
                if FIELD in person and person[FIELD]:
                    people.append(person)
        except:
            print(f'!! error while loading survey #{index}')
    return people

total = 0
missed = []

names = {}
counts = {}
emails = set()

fieldnames = ['last', 'first', 'email', 'numAttended']
output = csv.DictWriter(args.output, fieldnames=fieldnames)

def process(p):
    global names
    global counts
    # Get name
    name = p['name']
    print(f'considering {name}')
    if name not in counts:
        counts[name] = 1
    else:
        counts[name] += 1
    splitname = name.split()
    if len(splitname) < 2:
        names[p['name']] = {
            'first': p['name'],
            'last': "",
            'email': "",
            'numAttended': counts[p['name']]
        }
        return 'name is not full'

    # First and last
    first = splitname[0]
    last = splitname[-1]
    if len(first) == 1 or len(last) == 1:
        names[p['name']] = {
            'first': p['name'],
            'last': "",
            'email': "",
            'numAttended': counts[p['name']]
        }
        return 'name uses intials'
    name = first + ' ' + last


    # Get email
    email = p['email']
    if email is None:
        # CSM directory search
        print(f'  > finding email in csm directory')
        possible = list(csmdirsearch.search_by_name(name))
        if len(possible) > 1:
            return 'several possibilities'
        elif len(possible) == 0:
            names[p['name']] = {
                'first': first,
                'last': last,
                'email': 'Unknown',
                'numAttended': counts[p['name']]
            }
            return 'failed to find email'
        found = possible[0]
        print(f'  > found {found.name}')
        foundlast = found.name.last
        if foundlast.lower() == last.lower():
            email = found.business_email
            print(f'  > email is {email}')
        else:
            email = "Unknown"

    # Remove mymail
    email = email.replace('mymail.', '')

    # Deduplicate
    if email not in emails:
        emails.add(email)

    # Write to CSV
    print(f'  + outputting')
    names[p['name']] = {
        'first': first,
        'last': last,
        'email': email,
        'numAttended': counts[p['name']],
    }
    return None

# get_responses(args.auth, range(args.first, args.last + 1))
# exit()
for p in get_responses(args.auth, range(args.first, args.last + 1)):
    total += 1
    # try:
    err = process(p)
    if err is not None:
        print('  ! ' + err)
        name = p['name']
        missed.append((name, err))
    # except Exception as e:
    #     print(f'!! could not process person: {e}')
print(f'  + outputting')
for n in names:
    output.writerow(names[n])

print()
print(f'out of {total} records, {len(missed)} could not be processed:')
for (n, e) in set(missed):
    print(f'\t{n} : {e}')
