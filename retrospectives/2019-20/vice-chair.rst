===========
 Vice Chair
===========

:Author: David Florness

Firstly, thank you for volunteering to be the ACM Vice Chair!  The
responsibilities you'll be enduring will often be stressful, and most people
don't have the guts to step up to this kind of position.  That said, you'll have
the chance to meet a lot of smart people, have a very strong influence in the
direction of the club, and (I think) have fun along the way.

Here's a bit of advice for your role.  Feel free to contact me at anytime and
for any reason via Matrix or email <david@florness.com>.

Your primary responsibility is to schedule Tech Talks and, tangentially, to keep
a close eye on `the schedule
<https://gitlab.com/ColoradoSchoolOfMines/acm-schedule/>`_ to know what dates
you need to fill with talks.  Start planning these talks mid-summer.

Your secondary responsibility is keeping in close contact with Jack, making sure
he's doing OK, and asking how you can help.  This is something I didn't do
enough while I was an officer.  Jack is unbelievably diligent and usually takes
care of stuff without getting anyone else involved.  When you're not busy
thinking about talks, you should be offering him help without him asking.

Tech Talk Rules of Thumb
------------------------

I found that I was able to schedule better talks more easily when following
these basic rules.  Take each of these with a grain of salt.

1. **Don't be afraid to reach out to people whom you'd think would be unwilling
   or too busy.**

   I emailed `Matthew Hodgson <https://mastodon.matrix.org/@matthew>`_ and `Drew
   DeVault <https://drewdevault.com/>`_ with no expectations of getting a talk.
   I figured our chances were astronomically low with Matthew living in England
   and Drew living in Philadelphia.  Furthermore, I think these guys are
   legendary, and present at "real" conferences rather than lowly college clubs.

   It turns out the first problem could be worked around, and the second problem
   didn't exist.  Matthew was constantly traveling and was able to find a Friday
   in his crazy schedule where he happened to be in Colorado, and Drew was
   willing as long as he could have some help with flights.

   At the end of the day, there's no risk in emailing someone.  The worst they
   can do is laugh in your face and call you a hoser (which would be a great
   story to tell at Woody's anyways).  In general, people *want* to give talks.

2. **Rejection is normal.**

   People are busy, have commitments, or are just not interested.  Don't ever
   take it personally.

3. **Never talk to sales / HR when you don't have to.**

   We all know which of my talks last year was a complete shitshow, and it was
   primarily because I was in contact with a sales rep rather than an engineer.
   I was promised an engineer would come, give demos, etc, but at the last
   minute he couldn't make it.  Had I been talking to the engineer directly,
   things would've turned out much differently.  Engineers know how email works,
   so talk to them directly.  Unfortunately, for big companies (like C-MAPP
   sponsors), you often have to go through middlemen.

4. **Your introductory email should only be asking if they're interested.**

   You never want to throw out dates in the first email because if they take a
   long time to respond and you give out the same dates to someone else, they
   may finally respond with a date that's already been booked, resulting in
   conflicts.

5. **Stay in close contact with Dr. Dave <vdave@mines.edu>; she's your main
   point of contact for C-MAPP talks.**

   She'll let you know when she finds `C-MAPP <http://mapp.mines.edu/>`_ people
   who are interested in presenting.

6. **Keep the schedule spreadsheet with Dr. Camp and Dr. Dave updated.**

   This is entirely separate from the aforementioned acm-schedule repo (which I
   setup just for personal convenience).  People outside of the officers want to
   know who's talking and when so that they can do advertising.

7. **Remember that these are talks for your club, so aim for topics that you and
   your peers genuinely find interesting.**

8. **The day before a talk, email the presenter, letting him/her know when and
   where the talk is**

9. **Ask the presenter politely whether their company is willing to pay for
   food**
