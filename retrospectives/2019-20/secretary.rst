Secretary
#########

:Author: Jordan Newport

In this document I describe how things went as secretary of ACM for the 2019-20
school year.

Early planning
==============

Before the school year started, I had to figure out the sign-in form (on
acm.mines.edu) and room reservation (on aaiscloud).

Advertising
===========

Early in the year, we did some advertising in classes. I thought it went well,
since we were able to get newer students interested in the club. Make sure to
speak loudly, bring your laptop if you're not sure the professor does formal
slides, and be excited about ACM.

Scheduling Rooms
================

The interface for scheduling rooms is kind of a pain, but it does work. Make
sure you double check all the information you enter, to make sure that you
schedule the room for the right time and the right place.

One mistake I made was that one semester, I only reserved the room for one hour.
You want longer than that, so that the speaker isn't rushed to get out of the
room right at 7. Go until 8 unless there are no rooms available after 7:30, in
which case go until 7:30.

One resource you may find helpful is the PDF of room information, which includes
locations and sizes. It's the only place I've found specific capacity
information. It is slightly out of date, but it works surprisingly well despite
that.

Attendance Survey
=================

The survey is kind of a pain, because you need to scroll through all of your
meeting options to find the one you want. One option you have is to see if the
Mozzarella maintainer would be open to fixing this to make any sense at all
(even alphabetizing the list would be nice). The easier option you have is to
just pick a random meeting to assign your survey to, then figure out the details
later by assigning the meeting to your survey's ID. Make sure you leave yourself
enough time before the meeting that you can do this.
