Chair
#####

:Author: Jack Garner

In this document I describe how things went as chair of ACM for the 2018-19
school year.

Pre-Semester Planning
=====================

Planning was largely done on either side of summer. Here are some things that we
looked at over the summer and when they should probably get done in the future:

1. Transition meeting - First couple of weeks
2. Hackathon planning - Mid June - Mid July
3. Advertising - Late July - Early August
4. Project planning - Anytime

Advertising
===========

We advertised in various CS classes and at Celebration of Mines. This year we
primarily used Jack's old computers and the old maze solver program, but next
year it would be better if we had stuff owned by the club.

In class advertising was adhoc this year. We used a spreadsheet to
organize who was going in when, but we started pretty late on that. 


Tech Talks
==========

Tech Talks were great this year. At the beginning there were some oddities with
handling food that comes late. It's useful to have a title and short description
two weeks in advance for flyer making purposes.

BSO
===

We started the process of forwarding BSO confirmations around the officer team
which made sure things were getting submitted. Hopefully next year we'll have
funding again and can get more funding for travel.

General Notes
=============

- Remote meetings are really hard and don't really work well for pulling in
  a large number of members.
- The Git workshop was a lot of fun and we should do it again.
- HSPC still got planned late...
- Planning for the Hackathon was a LOT of work. Start as early as possible and
  have a list of attendees within the first few meetings. Don't go to anything
  earlier than November.
