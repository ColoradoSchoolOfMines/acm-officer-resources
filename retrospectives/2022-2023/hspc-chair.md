- You're already starting on this which is great. I started in July which we considered ahead (previous years we didn't start until winter break), so I have no doubt you'kk have a successful HSPC. Just be careful, as being ahead can let you feel a little complacent (we ended up doing some things last-minute because we felt like were ahead and had plenty of time)
- Over the summer:
	- Call for problems
	- Determining the scope of the competition for the next year (i.e. how many problems, beginner/advanced competition, etc.)
- Fall semester:
	- Continue recruiting problem authors
	- Review ideas as they are submitted
		- You want an interesting story and problem, with a core algorithm that is appropriate for high schoolers
		- Score the problem on the C/R/I scale, determine which competition(s) it is appropriate for: there are guidelines in the HSPC repo README
		- Let problem authors know when they can start developing the problem
	- Consider having "HSPC work days" for problem authors to have dedicated times to work on developing problems
	- Encourage problem authors to push up incomplete work so you can review early and often. Nobody wants to redo a bunch of work because they made bad assumptions
	- Consider having a spreadsheet to track problem status as they move from idea to in-progress to in-review to complete
	- Review problems as they are worked on
		- Concise, specific, and well-defined problem statement
		- Input verifier that matches ALL stated guarantees in the problem statement
		- Thorough test cases that test all edge cases, small inputs, large inputs, etc. At the very least, you should have 10 test cases
		- Leave comments, or make commits to change small things
		- Consider the inclusivity of stories and test cases (e.g. are all the stories about guys? Are the names in the sample inclusive?)
	- Try to get two *independent* solutions for each problem, and at least three solutions (especially for more difficult problems) . Let me know if I can be of help in this regard.
- Spring Semester:
	- Get feedback on problems from Rob, Gabe, and Dinesh. Send them problem descriptions and ask them to give feedback making sure the problems are clear
	- Determining which problems to actually use for the competition
	- Target getting problems to Greg Hammerly from Kattis 2 months in advanced of the competition
	- Advertising the competition
	- Creating a sign-up form/registration for participants
	- Start organizing the logistics for the competition
		- Recruiting volunteers (ACM, CS Weekly, Daily Blast, DecTech, ACM-W, previous competitors, etc.)
		- Reserving rooms for day-of
		- Minor protection forms, and everything involved with that
		- Having ITS prepare the images for the students to use
		- Determining and order prizes
		- Ordering food
		- Inviting the Dr. Bahar and PCJ to the event
	- Setting up the competition and practice competition on Kattis based on registration data
	- Volunteer orientation
- Consider how you want to advertise. This was something Dr. Camp did in previous years, and Christine did this year. Christine has lots of good connections with teachers, but consider advertising to students, or other avenues for reaching students. Leverage other K-12 outreach programs, advertise on social media, etc.
	- It would be cool to get an international team that actually competes!
- If you have any questions, or want advice, please don't hesitate to reach out to me. You have Jonathon, Sumner, and many faculty members to support you, but if there's anything I can do to help, please let me know. I would love to be involved with HSPC in some capacity, but I'm not sure what I'll have time for
