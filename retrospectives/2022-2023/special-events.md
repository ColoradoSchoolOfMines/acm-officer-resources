- Celebration of Mines
	- You must submit a form to claim a spot at the event. You can also submit a video. You all know this.
	- We have a table cloth in the ACM locker, we have used Jack Rosenthal's old Mac for mailing list sign-ups and something cool to show (also is usually the answer to an XC question for 101 in the fall), and Jonathon and Dorian made a music/light display that you could use again
	- You will need to ask for electricity if you use Jack's Mac
	- Primarily, be ready to sell ACM to all the students, and have a way for mailing list signups
		- We got 100 mailing list sign ups last year, so this is a major "recruiting" opportunity
	- Consider handing out ACM stickers
- Social events
	- We have historically done a South Table hike one of the first Saturdays of the Fall semester. This used to be with other computing clubs too, so consider collaborating with other clubs in that regard
	- Consider doing other social events: ACM-W has historically done a good job with these, with Kahoot trivia meetings and such
- Hackathons
	- We usually do an out-of-state hackathon in the Fall and Hack CU in the Spring. Jonathon is the only officer who has done an out-of-state hackathon, and knows what it looks like. But nobody on the officer team has organized it, so there will be a fair amount of learning with flight arrangments, etc. The CS department and SAIl should be able to help to some capacity.
	- You must meet with SAIL a month in advanced of the hackathon for traveling out-of-state
	- We typically have created sign-up sheets for people to let us know they're going so we can organize remote teams. This is probably more important for traveling hackathons to arrange ride, flights, etc.
- Git Workshop
	- Historically has been done on a Saturday, although you could consider incorporating it into one of the project meetings
	- There is a series of slides built with latex that has been used historically, Jonathon should know where
- Resume Workshop
	- We have done this for the past 2 years. Each iteration of the presentation should be in the drive. I would recommend updating it and then splitting parts among officers
	- Consider working with ACM-W on this. The whole workshop was originally built with ACM-W, and the first one we did at an ACM-W meeting
