Treasurer
#####

:Author: Colin Siles

My reflections on being the treasurer for the 2019-2020 school year

Treasurer Training
==================

I had to attend treasurer training near the beginning of the year in order to
start spending money on behalf of the club. There is A LOT of stuff in the
training, and they go over it very fast. There is also a quiz at the end to
prove you paid attention. Because of all the rules, I would recommend
contacting SAIL or BSO before making any purchases, just to make sure you don't
do something wrong.

COVID and Typical Treasurer Duties
==================================

Due to the COVID-19 pandemic, I did not do many of the tasks typically done by
a treasurer. We did not spend any money, because we could not do an
out-of-state hackathons, or order food (which are really the only two things we
spend money on).

I did fill out the allocation forms for the 2021-2022 school year (this happens
in March. Be aware there is a very fast turn-around time on the forms), and I
used past version of the form filled out by previous treasurers, as well as
conversations with the Chair (Jack Garner) to decide how to spend money for the
next year.

Chevron Donation
================

We received a $1000 donation from Chevron. We are required to use it before
August. We really didn't start consider spending it until March 2021, but I was
invovled in that process. We have ordered T-Shirts (which we can't use BSO
money for), and a Raspberry Pi Kit. We are planning to order a tablecloth for
Celebration of Mines and other events, as well as some stickers. I am still
working on getting these ordered as of the time of writing this.

ACM-W was able to order these things with little difficulty, but due to
personel changes in the CS department, we have had to go through an approval
process with SAIL and other Mines departments to get our T-Shirts approved.
The whole process took about a month, and we had to make modifications to our
deisgn to add the Mines Logo (although luckily we were allowed to use our ACM
logo that has a version of the Mines triangle on it). In the future, I would
recommend starting this type of process well in advanced of when you need
something.
