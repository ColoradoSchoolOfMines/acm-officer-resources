Treasurer
#########

:Author: Samuel Wafield

In this document I describe how things went as treasurer of ACM for the 2018-19
school year. Future treasurers pay close attention as ACM became huge this
year, introducing lots of new logistics.

Pre-Semester Planning
=====================

Over the summer, we did not have regular meetings. We did have a few sporadic
officer planning meetings, but other than that, we did not have any meetings.

It is important that the hackathons are choosen during these meetings. The
logistics are immense and need months of planning as detailed below in the
Hackathons section.

Expenditures
============

**This is your job.** You are the officer who handles money and knows how to use it.

Where to get Money
------------------
There are two(ish) places to get money. Mainly you will be dealing with the Computer
Science Department and the BSO. Here are some important points:

- Always use BSO funds first
    - Except for hackathons
- CS is very easy to use, just talk to the Department Coordinator. For 2019 it was
  Carol McEvoy
- Get Treasurer's training
- NEVER USE BSO FUNDS FOR HACKATHONS

Pizza
-----

The literal heartbeat of ACM. Don't mess with it.

It is a lure to get new members. Come for the pizza, stay for the people, code,
and or talks.

Cost saving my make it hard to decide where to cut expenses. Limiting pizza seems
like a good way to save

Limiting pizza (even by quantity) to is a 
slippery slope and something that if a new member heard about they wouldn't appreciate.
The core intention might not be bad, but I would caution doing things that could
cause a perception of exclusivity to anyone outside the club. Especially with the
complaints that ACM received at the last two CS Interrupts.

Personally I started at ACM because of the free pizza and stayed for the people
and the tech after the first couple of meetings. If the proposed were the case I
would have never joined ACM and wouldn't be as involved in the department. If
necessary cuts have to be made that's different, but to do it to discourage a this
small subset of behavior is not good. Especially with the additional risk to the
club's community


Celebration of Mines
--------------------

:GitLab Issue: `#9`_

Celebration of Mines is where we get the vast majority of our mailing list
singups. Thus, it is a vitally important recruiting event and we always need to
have a good showing.

There are a number of things which must happen and that you are responsible for
getting funding:

- Sign up for a table at Celebration. Make sure to get a table that has power
  for mailing list signups. Try and get a table next to LUG.
- **Get a bunch of ACM/Mozilla/Firefox stickers**.
- Make an ACM sign for the front of the table. We just had two :math:`11 \times
  17"` flyers with the Mines ACM logo on them.
- Create and print flyers with information about both ACM and LUG and about the
  first tech talk. This year, we printed 50 of each, and we ran out of them in
  the first hour. Printing 100-150 would have been better. We printed the tech
  talk flyers on half sheets and had the print shop cut them in half. We left
  the LUG/ACM info sheets together.

Tech Talks
==========

Generally, Robby (VC) handled organizing tech talks. It is the treasurer's job
to independently order pizza and have it arrive on time.

**On tech talk days:**

On each tech talk day, there's a lot of things that we have to manage. Here are
a few things that I made sure happened at every tech talk.

- Ensure that we have cups and plates.
- Order pizza in the morning.
    - Dominos or Anthony's
    - Use the Dominos rewards program
- Recruit 2-3 people to help manage the line

Hackathons
==========
Oh boy... You've got a roller coaster ahead of you...

Timeline:
- look for funding (Always)
- Pick a hackathon to goto (4-6 months)
- Talk about it at a meeting / send an email (2 months)
- Send Initail interest survey (2 months)
- Keep hyping at all meetings
    - Also remind people to sign up on the hackathon's stickers
- Send final registration survey (4 weeks out) **No turning back**
- Finalize funding and Expenditures (4 weeks out)
- Order flights and hotels (3 weeks out)
    - Call into the airline's group travel number
    - *Don't use the school's travel agent*

Research
========
https://web.archive.org/web/20190904014632/https://acm.mines.edu/research

BSO (Bull Shit Office)
======================
They do not like ACM, and are actively screw you over. Do not give them slack,
they will hang you with it.

I recommend that next year, you institute a policy that every single submission
must be entered into the acm-officer-resources repo and committed as it is being
submitted. The commit must also be GPG signed so that it can be
cryptographically verified that we submitted the form on time.
