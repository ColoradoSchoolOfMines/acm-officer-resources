Chair
#####

:Author: Sumner Evans

In this document I describe how things went as chair of ACM for the 2018-19
school year.

Pre-Semester Planning
=====================

Over the summer, we did not have regular meetings. We did have a few sporadic
officer planning meetings, but other than that, we did not have any meetings.

I also worked with Robby (Vice Chair) to organize `tech talks`_.

We also prepared for a large `advertising campaign <Advertising_>`_.

Advertising
===========

Close to the start of the semester, we began planning for our advertising
campaign. Our goal was to make sure that every freshman and sophomore knew about
ACM. (We assumed that most juniors and seniors should already know about ACM.)
Our campaign consisted of two main advertising methods:

1. Advertising in every entry-level CS class
2. Celebration of Mines

The campaign was extremely successful. We had 114 people sign up for our mailing
list. Given that the incoming class of freshmen was 1300 people, and assuming
that most of those signups were from freshmen, that's in the neigborhood of
5-7% of the freshmen class!

In-Class Advertisement
----------------------

:GitLab Issue: `#10`_

We went to every section of CSCI 101, 261, 262, 274, 303, and 306 to advertise
ACM. We figured that these were the classes that would most likely have freshmen
and sophomores in them.

The first thing that I did was create a spreadsheet with a list of all of the
sections of each of the entry level classes, their times, location, and who was
instructing them.

Then, I emailed all of the professors of each of the classes and asked them if
it was OK to pitch ACM for a minute during one of their classes on the first
week. After the professors confirmed, I made sure that at least one officer went
to each section.

Here is a link `the spreadsheet`_ that I set up to organize this.

.. _#10: https://gitlab.com/ColoradoSchoolOfMines/acm-planning/issues/10
.. _the spreadsheet: https://docs.google.com/spreadsheets/d/1J6n6xpouM9SfR03dDhU6qi4lv5RdWijxh31UjCEbrmQ/edit?usp=sharing

Celebration of Mines
--------------------

:GitLab Issue: `#9`_

Celebration of Mines is where we get the vast majority of our mailing list
singups. Thus, it is a vitally important recruiting event and we always need to
have a good showing.

There are a number of things which must happen:

- Sign up for a table at Celebration. Make sure to get a table that has power
  for mailing list signups. Try and get a table next to LUG.
- Get a bunch of ACM/Mozilla/Firefox stickers.
- Figure out some cool computer to use for mailing list singup. This year, we
  had Jack's Macintosh Plus.
- Make an ACM sign for the front of the table. We just had two :math:`11 \times
  17"` flyers with the Mines ACM logo on them.
- Work with Tracy Camp to get a question in the CSCI 101 scavenger hunt which
  encourages students to come by the ACM booth at Celebration. This year, the
  question was about what computer we were using for mailing list signups (it
  was a Macintosh Plus with 4MB of RAM).
- Create and print flyers with information about both ACM and LUG and about the
  first tech talk. This year, we printed 50 of each, and we ran out of them in
  the first hour. Printing 100-150 would have been better. We printed the tech
  talk flyers on half sheets and had the print shop cut them in half. We left
  the LUG/ACM info sheets together.
- Ensure that there are volunteers at the table at all times. This year, we
  probably had too many, but that's better than having too few!
- Have a short pitch for anyone who comes by the table that explains what the
  club is and encourage them to sign up for the mailing list. (Once they are on
  the list, they'll get emails that will hopefully continue to reel them in.)

.. _#9: https://gitlab.com/ColoradoSchoolOfMines/acm-planning/issues/9

Tech Talks
==========

Generally, Robby (VC) handled organizing tech talks.

**On tech talk days:**

On each tech talk day, there's a lot of people that we have to manage. Here are
a few things that I made sure happened at every tech talk.

- Ensure that we have cups and plates.
- Recruit 2-3 people to help manage the line (remove pizza boxes that are empty,
  make sure people aren't taking too much)
- Say hi to people (welcome people to the club)
- Short announcements and then introduce speaker

BSO
===

I recommend that next year, you institute a policy that every single submission
must be entered into the acm-officer-resources repo and committed as it is being
submitted. The commit must also be GPG signed so that it can be
cryptographically verified that we submitted the form on time.

General Notes
=============

- Spring has lower attendance. You may want to consider doing some sort of event
  to help bolster Spring semester attendance.
- Start planning HSPC very early. I can help out with this, and I think that
  Tracy mentioned that she wanted to get everything done before Christmas break
  which is a good idea.
- Overall, I think the year went well. The biggest challenge was trying to
  continue improving how people viewed the club. I think that there will always
  be negativity, and the trick is to figure out how to sort through the noise of
  complaints to find the real meat behind their grievances.
- I might suggest that you keep better notes throughout the semester as to what
  you do each week/month. That will make doing this retrospective easier or even
  not necessary.
