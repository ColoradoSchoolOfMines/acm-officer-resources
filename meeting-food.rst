Meeting Food
============

:Owner: Colin Siles

This is one of the most important responsibilities in Mines ACM.
Dr. Camp, the CS@Mines department head, has stated that it is important to her
that ACM provides food in order to attract members.

Purchasing Food
---------------

Keep track of all food purchase amounts, dates, and who paid for them 
in the Google Drive (see *Documentation* section).

Have the person responsible for scheduling tech talks ask presenters if they
are willing to pay for food. If so, either the presenter will order it 
themselves, or you will need to order pizza with CS@Mines funds and then have 
Carol send them an invoice for however much it cost.

From the school, you can either go through the BSO or through the CS department
to get money for pizza. In either case, you will be given a special credit card
to use. Return the card as soon as you are done placing the order.
**You must make sure the purchase is tax exempt, and you must keep an itemized
receipt for all purchases.**

The BSO requires that no more that 25% of the club's total BSO-funded
expenditures be spent on food. Pay careful attention to your budget.

Do your best to call Domino's 24 hours in advance. For tech talks, ask for the
pizza to be delivered 15 minutes prior to the start of the meeting. 
However, with project meetings, it is advisable to have pizza arrive in the
middle of the meeting to discourage people from just coming, grabbing food, and
leaving. You cannot make tax-exempt orders through the Domino's website, so you
**must** place the order by phone. Tip the delivery driver well, but with a 
strict upper limit of 20% of the original purchase amount, as that is a BSO rule.

How Much Pizza
--------------

This formula works well: *num_large_pizzas = math.ceil(2.5 * expected_attendance)*

You can use past attendance data from Mozzarella to work out the expected attendance.

Documentation
-------------

For each purchase you make, record the date, what was purchased, where it was 
purchased, the purchase amount, who paid for it, and the receipt in 
`this Google Sheet
<https://drive.google.com/drive/folders/0B9Olhz9tDXhcTkxhZTF3QjJrYzQ?usp=sharing>`_.
Make sure this stays private, only accessible by the officers.
