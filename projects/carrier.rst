Carrier Python
##############

Created in 2019 as a beginner project.

What is it?
===========

It's a project that, although filled with plenty of code, can be worked on
without reading any of the existing stuff. The app itself is a chatapp.

2019 retrospective
==================

Although fun, it was not as ideal for beginners as I would have liked. Although
the setup was documented, it still took most people an hour to get everything
working. I was the only one providing help on that project and it really
struggled as a result. There need to be more mentors on the project next year,
otherwise it won't be worth having.

The main lesson seems to be that although the code and dev environment are
important, it is significantly more important to have mentors.

An idea for next year would be to break members into small groups where they
meet up with their group outside of ACM time.
