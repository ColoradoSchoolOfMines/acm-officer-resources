let
  pkgs = import <nixpkgs> {};
  python38 = let
    packageOverrides = self: super: {
      # parso = super.parso.overridePythonAttrs(old: rec {
      #   version="0.7.0";
      #   src = super.fetchPypi {
      #     pname = old.pname;
      #     inherit version;
      #     sha256 = "908e9fae2144a076d72ae4e25539143d40b8e3eafbaeae03c1bfe226f4cdf12c";
      #   };
      # });
      # jedi = super.jedi.overridePythonAttrs(old: rec {
      #   version="0.17.0";
      #   src = super.fetchPypi {
      #     pname = old.pname;
      #     inherit version;
      #     sha256 = "df40c97641cb943661d2db4c33c2e1ff75d491189423249e989bcea4464f3030";
      #   };
      # });
    };
    in pkgs.python38.override {inherit packageOverrides;};
  # sphinx-autobuild = pkgs.python3.pkgs.buildPythonPackage rec {
  #     pname = "sphinx-autobuild";
  #     version = "0.7.1";

  #     src = pkgs.python3.pkgs.fetchPypi {
  #       inherit pname version;
  #       sha256 = "66388f81884666e3821edbe05dd53a0cfb68093873d17320d0610de8db28c74e";
  #     };

  #     doCheck = false;
  #     propagatedBuildInputs = with pkgs.python3Packages; [ tornado watchdog livereload ];
  # };
  csm-dirsearch = pkgs.python3.pkgs.buildPythonPackage rec {
      pname = "csmdirsearch";
      version = "0.1.1";

      src = pkgs.python3.pkgs.fetchPypi {
        inherit pname version;
        sha256 = "b44539ab0fbd135aade9c99d9c0c4508870cafd4fcec70a01a1c760a5731a760";
      };

      doCheck = false;
      propagatedBuildInputs = with pkgs.python3Packages; [requests beautifulsoup4];
  };
  # rstcheck = pkgs.python3.pkgs.buildPythonPackage rec {
  #     pname = "rstcheck";
  #     version = "3.3.1";

  #     src = pkgs.python3.pkgs.fetchPypi {
  #       inherit pname version;
  #       sha256 = "07vl2p16fw0jayfcn424ixfc3cyhj8pnm89b83h70hm5as9ggi4j";
  #     };

  #   propagatedBuildInputs = with python38.pkgs; [ requests beautifulsoup4];
  #   # propogatedBuildInputs = with python38.pkgs; [ requests beautifulsoup4];
  #   # checkPhase = ''
  #   #   echo "Don't run tests"
  #   # '';
  # };
in
pkgs.mkShell {
  buildInputs = [
    (pkgs.python3.withPackages (ps: with ps; [ requests jedi pylint pynvim toml beautifulsoup4 csm-dirsearch certifi]))
  ];
}

# let
#   pkgs = import <nixpkgs> {};
#   python38 = let
#     packageOverrides = self: super: {
#       parso = super.parso.overridePythonAttrs(old: rec {
#         version="0.7.0";
#         src = super.fetchPypi {
#           pname = old.pname;
#           inherit version;
#           sha256 = "908e9fae2144a076d72ae4e25539143d40b8e3eafbaeae03c1bfe226f4cdf12c";
#         };
#       });
#       jedi = super.jedi.overridePythonAttrs(old: rec {
#         version="0.17.0";
#         src = super.fetchPypi {
#           pname = old.pname;
#           inherit version;
#           sha256 = "df40c97641cb943661d2db4c33c2e1ff75d491189423249e989bcea4464f3030";
#         };
#       });
#     };
#     in pkgs.python38.override {inherit packageOverrides;};
#   port-for = pkgs.python38.pkgs.buildPythonPackage rec {
#       pname = "port-for";
#       version = "0.3.1";

#       src = pkgs.python38.pkgs.fetchPypi {
#         inherit pname version;
#         sha256 = "073gb767blwhdg0xw61rwckrr735gjqkigi99js4v5f256xq8smi";
#       };

#       doCheck = false;
#       propagatedBuildInputs = with pkgs.python38Packages; [ ];
#   };
#   sphinx-autobuild = pkgs.python38.pkgs.buildPythonPackage rec {
#       pname = "sphinx-autobuild";
#       version = "0.7.1";

#       src = pkgs.python38.pkgs.fetchPypi {
#         inherit pname version;
#         sha256 = "66388f81884666e3821edbe05dd53a0cfb68093873d17320d0610de8db28c74e";
#       };

#       doCheck = false;
#       propagatedBuildInputs = with pkgs.python38Packages; [ tornado watchdog port-for livereload ];
#   };
#   rstcheck = pkgs.python38.pkgs.buildPythonPackage rec {
#       pname = "rstcheck";
#       version = "3.3.1";

#       src = pkgs.python38.pkgs.fetchPypi {
#         inherit pname version;
#         sha256 = "07vl2p16fw0jayfcn424ixfc3cyhj8pnm89b83h70hm5as9ggi4j";
#       };

#       doCheck = false;
#       propagatedBuildInputs = with pkgs.python38Packages; [ docutils ];
#   };
# in
# pkgs.mkShell {
#   buildInputs = [
#     (python38.withPackages (ps: with ps; [ requests jedi pylint pynvim toml sphinx sphinx-autobuild rstcheck sphinx_rtd_theme ]))
#   ];
# }
